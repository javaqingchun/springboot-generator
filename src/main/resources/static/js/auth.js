function refreshAToken(){
    $("a").each(function () {
        var href = $(this).attr("href");
        if (href&&href.length > 1) {
            var token = sessionStorage.getItem("token");
            if (token&&token!=null&&token!='null') {
                if (href.indexOf("?") > -1) {
                    href = href + "&token=" + token;
                } else {
                    href = href + "?token=" + token;
                }
            }
            $(this).attr("href",href);
        }
    });
}


$.ajaxSetup({
    beforeSend: function (xhr) {
        if (arguments[1].url) {
            if (arguments[1].url.indexOf("/v1/auths/login") > -1) {
                return;
            }
        }
        var token = sessionStorage.getItem("token");
        if (token&&token!=null&&token!='null') {
            xhr.setRequestHeader('token', token)
        }

    }

});
refreshAToken();
refreshAndGetAuthenticationToken();
setInterval(
    function(){
        refreshAndGetAuthenticationToken();
    }   ,
    10000
);
function refreshAndGetAuthenticationToken() {

    //5分钟刷新一次tocken
    var token = sessionStorage.getItem("token");
    if (token&&token!=null&&token!='null') {
       var tokenRefreshTime = sessionStorage.getItem("tokenRefreshTime");
       if(tokenRefreshTime&&tokenRefreshTime!=null&&tokenRefreshTime!='null'){
           var timestamp = (new Date()).valueOf();
           var time=timestamp-tokenRefreshTime;
           console.log("上次刷新时间:"+tokenRefreshTime+",当前时间:"+timestamp+"相差毫秒数:"+time);
           if(time>290000){ //10秒刷新token
               console.log("刷新token");
               shuaxinToken();
           }
       }else{
           shuaxinToken();
       }

    }

}
function shuaxinToken(){
    $.ajax({
        type: 'GET',
        url: '/v1/auths/refresh',
        dataType: 'json',
        success: function (data) {
            if (data != null && data.data != null && data.code == 200) {
                sessionStorage.setItem("token", data.data);
                sessionStorage.setItem("tokenRefreshTime",(new Date()).valueOf());
                refreshAToken();
            } else {
                alert("刷新token失败" + data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var json = JSON.parse(jqXHR.responseText);
           // alert(json.message);
            if(json.code&&json.code=="error.401"){
                location.href="/index.html";
                sessionStorage.setItem("token", null);
                sessionStorage.setItem("tokenRefreshTime",null);
            }
        }
    });
}
//绑定beforeunload事件
$(window).bind('beforeunload',function(){
    var url =location.href;
    var star=-1;

    if(url.indexOf("token=")>-1){
        star=url.indexOf("token=");
    }
    if(star!=-1){
        url= url.substring(star);
        if(url.indexOf("&")>-1){
            url= url.substring(0,url.indexOf("&"));

        }
        var token = sessionStorage.getItem("token");
        if (token&&token!=null&&token!='null') {
            location.href =  location.href.replace(url,"token="+token);
        }
    }


});