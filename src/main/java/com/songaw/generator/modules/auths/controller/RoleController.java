package com.songaw.generator.modules.auths.controller;

import com.github.pagehelper.PageHelper;
import com.songaw.generator.common.exception.BadRequestException;
import com.songaw.generator.common.pojo.dto.PageInfoDto;
import com.songaw.generator.common.pojo.dto.Result;
import com.songaw.generator.modules.auths.entity.Role;
import com.songaw.generator.modules.auths.pojo.dto.RoleDto;
import com.songaw.generator.modules.auths.pojo.vo.AddRoleVo;
import com.songaw.generator.modules.auths.pojo.vo.SearchRoleVo;
import com.songaw.generator.modules.auths.pojo.vo.UpdateRoleVo;
import com.songaw.generator.modules.auths.service.RoleService;
import io.swagger.annotations.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
* TODO
*
* @author songaw
* @date 2018-13-08  11:13:29
*/
@Api(value = "Role接口", tags = { "Role接口" })
@RestController
@RequestMapping("/v1/roles")
public class RoleController {
@Autowired
RoleService roleService;


    /**
    * 添加
    * @param vo
    */
    @ApiOperation(value = "添加Role", notes = "添加role")
    @ApiImplicitParam(name = "vo", value = "addRoleVo", required = true, dataType = "AddRoleVo")
    @ApiResponses({ @ApiResponse(code = 400, message = "请求参数错误"), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径error", response = Result.class) })
    @PostMapping
    public Result<RoleDto> addRole(@RequestBody AddRoleVo vo) {
        return Result.getSuccessResult(roleService.addRoleAndMenus(vo));
    }
    @ApiOperation(value = "修改Role", notes = "修改role")
    @ApiImplicitParam(name = "vo", value = "updateRoleVo", required = true, dataType = "UpdateRoleVo")
    @ApiResponses({ @ApiResponse(code = 400, message = "请求参数错误"), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径error", response = Result.class) })
    @PutMapping
    public Result updateRole(@RequestBody UpdateRoleVo vo){
            Long id=vo.getId();
            if(id==null){
               throw new BadRequestException("error.0001","ID");
            }
            roleService.updateRoleAndMenus(vo);
            return Result.getSuccessResult(null);

    }
    @ApiOperation(value = "删除 Role ", notes = "删除 role ")
    @ApiImplicitParam(name = "id", value = "Roleid", required = true, dataType = "String")
    @ApiResponses({ @ApiResponse(code = 400, message = "请求参数错误 "), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径error   ", response = Result.class) })
    @DeleteMapping(value = "/{id}")
    public Result deleteRoleById(@PathVariable("id") Long id){
        try {
        Role role =roleService.selectByPk(id);
            if(role!=null){
                roleService.deleteByPk(id);
            }else{
                return Result.getSystemErrorResult(" 找不到要删除的数 ");
            }

            return Result.getSuccessResult(null);
        }catch (Exception e){
            return Result.getSystemErrorResult(e.getMessage());
        }
    }
    @ApiOperation(value = "删除列表 Roles", notes = "删除列表 role ")
    @ApiImplicitParam(name = "ids", value = "roleids:id1,id2,id3", required = true, dataType = "String")
    @ApiResponses({ @ApiResponse(code = 400, message = "请求参数错误 "), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径error   ", response = Result.class) })
    @DeleteMapping(value = "/deleteUserByIds")
    public Result deleteRoleByIds(@RequestParam("ids") String ids){
        try {
            String [] idstr=ids.split(",");
            List<Long> idList = new ArrayList<>();
            for(String id:idstr){
                idList.add(Long.parseLong(id));
            }
            roleService.deleteByPks(idList);
            return Result.getSuccessResult(null);
        }catch (Exception e){
            return Result.getSystemErrorResult(e.getMessage());
        }
    }
    @ApiOperation("根据ID查询信息")
    @GetMapping(value = "/{id}")
    @ApiImplicitParam(name = "id", value = "RoleID", dataType = "Long", paramType = "query")
    @ResponseBody
    public Result<RoleDto> getRoleById(@PathVariable("id") Long id) {
        RoleDto roleDto = roleService.findById(id);
        return Result.getSuccessResult(roleDto);
    }
    @ApiOperation(value = " 列表查询 ", notes = " 列表查询 ")
    @ApiResponses({ @ApiResponse(code = 400, message = "请求参数错误 "), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径error   ", response = Result.class) })
    @RequestMapping(path = "/list",method = RequestMethod.GET)
    public Result<List<Role>> findRoles(@ModelAttribute SearchRoleVo searchVo){
        Role searchRole = new Role();
        BeanUtils.copyProperties(searchVo,searchRole);
        List<Role>  list=  roleService.select(searchRole);
        return Result.getSuccessResult(list);
    }

    @ApiOperation(value = " 分页列表查询 ", notes = " 分页列表查询 ")
        @ApiResponses({ @ApiResponse(code = 400, message = "请求参数错误 "), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径error   ", response = Result.class) })
    @RequestMapping(path = "/list/search",method = RequestMethod.GET)
    public Result<PageInfoDto> findRolesPage(@ModelAttribute SearchRoleVo searchVo){
        PageHelper.startPage(searchVo.getPageIndex(),searchVo.getPageSize());
        Role searchRole = new Role();
        BeanUtils.copyProperties(searchVo,searchRole);
        List<Role>  list=  roleService.select(searchRole);
        PageInfoDto<Role> thePage=new PageInfoDto<Role>(list);
        return Result.getSuccessResult(thePage);
    }


}
