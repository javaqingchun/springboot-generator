package com.songaw.generator.modules.auths.pojo.dto;

import com.songaw.generator.modules.auths.entity.Role;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
 * 权限相关的角色信息
 *
 * @author songaw
 * @date 2018/7/27 15:18
 */
@Data
public class RoleDto extends Role implements  GrantedAuthority {
    private static final long serialVersionUID = 7938815305887155880L;
    List<MenuDto> menus;
    @Override
    public String getAuthority() {
        return getCode();
    }
}
