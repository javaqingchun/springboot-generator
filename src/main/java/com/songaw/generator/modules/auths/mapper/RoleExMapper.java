package com.songaw.generator.modules.auths.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * TODO
 *
 * @author songaw
 * @date 2018/8/8 13:58
 */
public interface RoleExMapper {
    public void insertBatchRoleJoinMenu(@Param("roleId") Long roleId, @Param("menuIds") List<Long> menuIds) ;

    void deleteRoleJoinMenu(Long id);
}
