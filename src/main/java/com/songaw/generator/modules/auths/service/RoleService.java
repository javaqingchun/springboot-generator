package com.songaw.generator.modules.auths.service;

import com.songaw.generator.common.service.BaseService;
import com.songaw.generator.modules.auths.entity.Role;
import com.songaw.generator.modules.auths.pojo.dto.RoleDto;
import com.songaw.generator.modules.auths.pojo.vo.AddRoleVo;
import com.songaw.generator.modules.auths.pojo.vo.UpdateRoleVo;
import org.springframework.stereotype.Service;


/**
* TODO
*
* @author songaw
* @date 2018-13-08  11:13:29
*/
@Service
public interface RoleService extends BaseService<Role,Long> {
    RoleDto addRoleAndMenus(AddRoleVo addRoleVo);
    void updateRoleAndMenus(UpdateRoleVo updateRoleVo);
    RoleDto findById(Long id);
}
