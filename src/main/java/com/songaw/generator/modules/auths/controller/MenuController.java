package com.songaw.generator.modules.auths.controller;

import com.songaw.generator.common.pojo.dto.Result;
import com.songaw.generator.modules.auths.entity.Menu;
import com.songaw.generator.modules.auths.pojo.dto.MenuDto;
import com.songaw.generator.modules.auths.pojo.vo.AddMenuVo;
import com.songaw.generator.modules.auths.pojo.vo.UpdateMenuVo;
import com.songaw.generator.modules.auths.security.TreeMenuBuilder;
import com.songaw.generator.modules.auths.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO
 *
 * @author songaw
 * @date 2018/8/1 10:48
 */
@Api(value = "菜单管理接口", tags = { "菜单管理接口" })
@RestController
@RequestMapping("/v1/menus")
public class MenuController {
    @Autowired
    MenuService menuService;
    @RequestMapping(
            value = "/list",
            method = RequestMethod.GET)
    public Result<List<Menu>> list(){
        Example example = new Example(Menu.class);
        example.setOrderByClause("`parent_ids` ASC,sort ASC");
      List<Menu> list=  menuService.selectByExample(example);
      return Result.getSuccessResult(list);
    }
    @PostMapping
    public Result addMenuAndApis(@Valid @RequestBody AddMenuVo addMenuVo){
        menuService.addMenuAndApis(addMenuVo);
        return Result.getSuccessResult("添加成功");
    }
    @PutMapping
    public Result updateMenuAndApis(@Valid @RequestBody UpdateMenuVo updateMenuVo){
        menuService.updateMenuAndApis(updateMenuVo);
        return Result.getSuccessResult("添加成功");
    }
    //获取apidos
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.GET)
    @ResponseBody
    public Result findById(@PathVariable("id") Long id) {
        MenuDto menuDto=menuService.findById(id);

        return Result.getSuccessResult(menuDto);
    }
    //获取apidos
    @RequestMapping(
            value = "/{id}",
            method = RequestMethod.DELETE)
    @ResponseBody
    public Result deleteMenuById(@PathVariable("id") Long id) {

        menuService.deleteMenuById(id);
        return Result.getSuccessResult(null);
    }
    @ApiOperation(value = "获取所有的菜单树形结构", notes = "获取所有的菜单树形结构")
    @ApiResponses({ @ApiResponse(code = 400, message = "请求参数没填好"), @ApiResponse(code = 404, message = "请求路径没有或页面跳转路径不对", response = Result.class) })
    @RequestMapping(path = "/trees",method = RequestMethod.GET)
    public Result<List<MenuDto>> trees(){
        Example example = new Example(Menu.class);
        example.setOrderByClause("`parent_ids` ASC,sort ASC");
        List<Menu> menuDtos=  menuService.selectByExample(example);
        menuDtos=menuDtos.stream().filter(item->item.getIsShow()).collect(Collectors.toList());
        List<MenuDto> list = new ArrayList<>();
        for(Menu menu:menuDtos){
            MenuDto menuDto = new MenuDto();
            BeanUtils.copyProperties(menu,menuDto);
            list.add(menuDto);
        }
        list= TreeMenuBuilder.bulid(list);
        return Result.getSuccessResult(list);
    }
}
