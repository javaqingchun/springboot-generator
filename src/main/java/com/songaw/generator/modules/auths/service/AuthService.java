package com.songaw.generator.modules.auths.service;

import com.songaw.generator.common.pojo.dto.Result;
import com.songaw.generator.modules.auths.pojo.dto.MenuDto;
import com.songaw.generator.modules.auths.pojo.vo.AddUserVo;
import com.songaw.generator.modules.auths.pojo.vo.JwtLoginVo;

import java.util.List;

/**
 * TODO
 *
 * @author songaw
 * @date 2018/7/27 17:32
 */
public interface AuthService {
    Result register(AddUserVo userToAdd);
    Result login(JwtLoginVo authenticationRequest);
    //提供给前端使用
    String refresh(String oldToken);
    //提供给过滤器使用
    String refresh(String authToken, String username);
    List<MenuDto> getMenusByUserId(Long userId);
}
