package com.songaw.generator.modules.auths.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * TODO
 *
 * @author songaw
 * @date 2018-13-08  11:13:29
 */
@ApiModel(value = "AddRoleVo")
@Data
public class AddRoleVo {
    /**
     * 角色名称
     */
    @NotNull
    @ApiModelProperty(value = "角色名称")
    private String name;

    /**
     * 角色权限标识
     */
    @NotNull
    @ApiModelProperty(value = "角色code")
    private String code;

    @ApiModelProperty(value = "菜单")
    List<Long> menuIds;

}
