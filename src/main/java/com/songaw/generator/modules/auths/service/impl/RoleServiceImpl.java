package com.songaw.generator.modules.auths.service.impl;

import com.songaw.generator.common.service.impl.BaseServiceImpl;
import com.songaw.generator.modules.auths.entity.Role;
import com.songaw.generator.modules.auths.mapper.MenuExMapper;
import com.songaw.generator.modules.auths.mapper.RoleExMapper;
import com.songaw.generator.modules.auths.mapper.RoleMapper;
import com.songaw.generator.modules.auths.pojo.dto.MenuDto;
import com.songaw.generator.modules.auths.pojo.dto.RoleDto;
import com.songaw.generator.modules.auths.pojo.vo.AddRoleVo;
import com.songaw.generator.modules.auths.pojo.vo.UpdateRoleVo;
import com.songaw.generator.modules.auths.service.RoleService;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* TODO
*
* @author songaw
* @date 2018-13-08  11:13:29
*/
@Data
@Service
public class RoleServiceImpl extends BaseServiceImpl<Role,Long> implements RoleService  {

    @Autowired
    RoleMapper baseMapper;
    @Autowired
    RoleExMapper roleExMapper;
    @Autowired
    MenuExMapper menuExMapper;
    @Override
    public RoleDto addRoleAndMenus(AddRoleVo addRoleVo) {
        Role role = new Role();
        BeanUtils.copyProperties(addRoleVo,role);
        insert(role);
        RoleDto roleDto = new  RoleDto();
        BeanUtils.copyProperties(role,roleDto);
        if(addRoleVo.getMenuIds().size()>0) {
            roleExMapper.insertBatchRoleJoinMenu(role.getId(), addRoleVo.getMenuIds());
        }
        return  roleDto;
    }

    @Override
    public void updateRoleAndMenus(UpdateRoleVo updateRoleVo) {
        Role role = new Role();
        BeanUtils.copyProperties(updateRoleVo,role);
        update(role);
        //apis
        roleExMapper.deleteRoleJoinMenu(role.getId());
        if(updateRoleVo.getMenuIds().size()>0) {

            roleExMapper.insertBatchRoleJoinMenu(role.getId(), updateRoleVo.getMenuIds());
        }

    }
    @Override
    public RoleDto findById(Long id) {
        Role role = baseMapper.selectByPrimaryKey(id);
        RoleDto roleDto = new RoleDto();
        BeanUtils.copyProperties(role,roleDto);
        List<MenuDto> menuDtos= menuExMapper.findMenuByRoleId(id);
        roleDto.setMenus(menuDtos);
        return roleDto;
    }


}