package com.songaw.generator.modules.globlecache.service.impl;

import com.songaw.generator.common.constant.CacheConstant;
import com.songaw.generator.common.service.impl.BaseServiceImpl;
import com.songaw.generator.modules.globlecache.entity.CacheTable;
import com.songaw.generator.modules.globlecache.mapper.CacheTableMapper;
import com.songaw.generator.modules.globlecache.service.CacheTableService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
* TODO
*
* @author songaw
* @date 2018-21-08  14:21:26
*/
@Data
@Service
public class CacheTableServiceImpl extends BaseServiceImpl<CacheTable,Long> implements CacheTableService  {

    @Autowired
    CacheTableMapper baseMapper;

    @Override

    @Cacheable(value= CacheConstant.CACHE_ALL)
    public List<CacheTable> getCacheTableList() {
        Example example = new Example(CacheTable.class);
        example.setOrderByClause("`parent_name` ASC,insert_time ASC");
        return baseMapper.selectByExample(example);
    }
}