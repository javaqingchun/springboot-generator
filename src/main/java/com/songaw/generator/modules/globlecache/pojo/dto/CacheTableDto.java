package com.songaw.generator.modules.globlecache.pojo.dto;

import com.songaw.generator.modules.globlecache.entity.CacheTable;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
* TODO
*
* @author songaw
* @date 2018-21-08  14:21:26
*/
@ApiModel(value = "CacheTableDto")
@Data
public class CacheTableDto  extends CacheTable implements Serializable {
    private static final long serialVersionUID = 1L;
}
