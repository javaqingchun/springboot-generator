package com.songaw.generator.modules.globlecache.pojo.dto;

import lombok.Data;

/**
 * TODO
 *
 * @author songaw
 * @date 2018/9/19 10:55
 */
@Data
public class CacheDto {
    private String cacheName;
    private String memo;
    private String updateTime;
}
