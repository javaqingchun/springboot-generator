package com.songaw.generator.modules.globlecache.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * TODO
 *
 * @author songaw
 * @date 2018-21-08  14:21:26
 */
@ApiModel(value = "AddCacheTableVo")
@Data
public class AddCacheTableVo {
    @ApiModelProperty(value = "缓存名称")
    private String name;

    /**
     * 备注
     */
    @ApiModelProperty(value = "`备注`")
    private String memo;


    /**
     * 依赖的父级缓存名称
     */
    @ApiModelProperty(value = "`依赖的父级缓存名称`")
    private String parentName;

    @ApiModelProperty(value = "`缓存类型`")
    private String type;

    /**
     * 缓存时间(秒)
     */
    @ApiModelProperty(value = "`缓存时间(秒)`")
    private Long cacheTime;
}
