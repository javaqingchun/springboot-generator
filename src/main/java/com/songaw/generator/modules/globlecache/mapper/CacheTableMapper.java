package com.songaw.generator.modules.globlecache.mapper;

import com.songaw.generator.common.mapper.MybatisMapper;
import com.songaw.generator.modules.globlecache.entity.CacheTable;

public interface CacheTableMapper extends MybatisMapper<CacheTable> {
}