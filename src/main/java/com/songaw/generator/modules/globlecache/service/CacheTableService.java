package com.songaw.generator.modules.globlecache.service;

import com.songaw.generator.common.service.BaseService;
import com.songaw.generator.modules.globlecache.entity.CacheTable;
import org.springframework.stereotype.Service;

import java.util.List;


/**
* TODO
*
* @author songaw
* @date 2018-21-08  14:21:26
*/
@Service
public interface CacheTableService extends BaseService<CacheTable,Long> {
    List<CacheTable> getCacheTableList();
}
