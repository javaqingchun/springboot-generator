package com.songaw.generator.modules.globlecache.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "`gg_cache_table`")
public class CacheTable {
    @Id
    @Column(name = "`id`")
    private Long id;

    @Column(name = "`name`")
    private String name;

    /**
     * 备注
     */
    @Column(name = "`memo`")
    private String memo;

    /**
     * 上次更新日期
     */
    @Column(name = "`old_update_time`")
    private Date oldUpdateTime;

    /**
     * 依赖的父级缓存名称
     */
    @Column(name = "`parent_name`")
    private String parentName;

    @Column(name = "`type`")
    private String type;

    /**
     * 缓存时间(秒)
     */
    @Column(name = "`cache_time`")
    private Long cacheTime;

    @Column(name = "`UPDATE_USER`")
    private Long updateUser;

    @Column(name = "`UPDATE_TIME`")
    private Long updateTime;

    @Column(name = "`INSERT_USER`")
    private Long insertUser;

    @Column(name = "`INSERT_TIME`")
    private Long insertTime;

    @Column(name = "`DELETE_FLAG`")
    private String deleteFlag;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取备注
     *
     * @return memo - 备注
     */
    public String getMemo() {
        return memo;
    }

    /**
     * 设置备注
     *
     * @param memo 备注
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * 获取上次更新日期
     *
     * @return old_update_time - 上次更新日期
     */
    public Date getOldUpdateTime() {
        return oldUpdateTime;
    }

    /**
     * 设置上次更新日期
     *
     * @param oldUpdateTime 上次更新日期
     */
    public void setOldUpdateTime(Date oldUpdateTime) {
        this.oldUpdateTime = oldUpdateTime;
    }

    /**
     * 获取依赖的父级缓存名称
     *
     * @return parent_name - 依赖的父级缓存名称
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * 设置依赖的父级缓存名称
     *
     * @param parentName 依赖的父级缓存名称
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取缓存时间(秒)
     *
     * @return cache_time - 缓存时间(秒)
     */
    public Long getCacheTime() {
        return cacheTime;
    }

    /**
     * 设置缓存时间(秒)
     *
     * @param cacheTime 缓存时间(秒)
     */
    public void setCacheTime(Long cacheTime) {
        this.cacheTime = cacheTime;
    }

    /**
     * @return UPDATE_USER
     */
    public Long getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser
     */
    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * @return UPDATE_TIME
     */
    public Long getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return INSERT_USER
     */
    public Long getInsertUser() {
        return insertUser;
    }

    /**
     * @param insertUser
     */
    public void setInsertUser(Long insertUser) {
        this.insertUser = insertUser;
    }

    /**
     * @return INSERT_TIME
     */
    public Long getInsertTime() {
        return insertTime;
    }

    /**
     * @param insertTime
     */
    public void setInsertTime(Long insertTime) {
        this.insertTime = insertTime;
    }

    /**
     * @return DELETE_FLAG
     */
    public String getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * @param deleteFlag
     */
    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}