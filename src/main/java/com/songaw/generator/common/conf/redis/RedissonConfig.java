package com.songaw.generator.common.conf.redis;

import io.netty.channel.nio.NioEventLoopGroup;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.Codec;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

/**
 * Created by kl on 2017/09/26.
 * redisson 客户端配置
 */
@Component
@Configuration
public class RedissonConfig {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private Integer port;
    @Value("${spring.redis.password}")
    private String password;

    @Value("${spring.redis.pool.min-idle}")
    private int connectionMinimumIdleSize = 10;//从节点最小空闲连接数）
    private int idleConnectionTimeout = 10000;
    private int pingTimeout = 1000;
    private int connectTimeout = 10000;
    @Value("${spring.redis.timeout}")
    private int timeout = 3000;
    private int retryAttempts = 3;//命令失败重试次数）
    private int retryInterval = 1500; //命令重试发送时间间隔，单位：毫秒）
    private int reconnectionTimeout = 3000;//重新连接时间间隔，单位：毫秒）
    private int failedAttempts = 3;//执行失败最大次数）

    private int subscriptionsPerConnection = 5;//单个连接最大订阅数量
    private String clientName = null; //名称
    @Value("${spring.redis.pool.min-idle}")
    private int subscriptionConnectionMinimumIdleSize = 1; //从节点发布和订阅连接的最小空闲连接数）
    @Value("${spring.redis.pool.max-active}")
    private int subscriptionConnectionPoolSize = 50; //（从节点发布和订阅连接池大小）
    @Value("${spring.redis.pool.max-idle}")
    private int connectionPoolSize = 64;//连接池大小）
    @Value("${spring.redis.database}")
    private int database = 0;
    private int dnsMonitoringInterval = 5000;//（DNS监测时间间隔，单位：毫秒）

    private int thread=0; //当前处理核数量 * 2


    @Bean(destroyMethod = "shutdown")
    RedissonClient redissonClient() throws Exception {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://"+host+":"+port)
                .setConnectionMinimumIdleSize(connectionMinimumIdleSize)
                .setConnectionPoolSize(connectionPoolSize)
                .setDatabase(database)
               // .setDnsMonitoring(dnsMonitoring)
                .setDnsMonitoringInterval(dnsMonitoringInterval)
                .setSubscriptionConnectionMinimumIdleSize(subscriptionConnectionMinimumIdleSize)
                .setSubscriptionConnectionPoolSize(subscriptionConnectionPoolSize)
                .setSubscriptionsPerConnection(subscriptionsPerConnection)
                .setClientName(clientName)
                .setFailedAttempts(failedAttempts)
                .setRetryAttempts(retryAttempts)
                .setRetryInterval(retryInterval)
                .setReconnectionTimeout(reconnectionTimeout)
                .setTimeout(timeout)
                .setConnectTimeout(connectTimeout)
                .setIdleConnectionTimeout(idleConnectionTimeout)
                .setPingTimeout(pingTimeout)
                .setPassword(password);
        Codec codec = (Codec) ClassUtils.forName("org.redisson.codec.JsonJacksonCodec", ClassUtils.getDefaultClassLoader()).newInstance();
        config.setCodec(codec);
        config.setThreads(thread);
        config.setEventLoopGroup(new NioEventLoopGroup());
        return Redisson.create(config);
    }
}