package com.songaw.generator.common.conf.redis;


/**
 * @author Anwei.S
 * @CreateDate 2017年2月23日 上午11:10:08
 *
 */
public interface CacheLoadback<T> {
	T load() throws Exception;
}
