package com.songaw.generator.common.conf.redis;

import com.alibaba.fastjson.parser.ParserConfig;
import com.songaw.generator.common.util.ApplicationContextUtils;
import com.songaw.generator.modules.globlecache.entity.CacheTable;
import com.songaw.generator.modules.globlecache.service.CacheTableService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author songaw
 */
@Configuration
public class RedisConfig {
 
    /**
     * 重写Redis序列化方式，使用Json方式:
     * 当我们的数据存储到Redis的时候，我们的键（key）和值（value）都是通过Spring提供的Serializer序列化到数据库的。RedisTemplate默认使用的是JdkSerializationRedisSerializer，StringRedisTemplate默认使用的是StringRedisSerializer。
     * Spring Data JPA为我们提供了下面的Serializer：
     * GenericToStringSerializer、Jackson2JsonRedisSerializer、JacksonJsonRedisSerializer、JdkSerializationRedisSerializer、OxmSerializer、StringRedisSerializer。
     * 在此我们将自己配置RedisTemplate并定义Serializer。
     * Primary 在autoware时优先选用我注册的bean.
     * 因为在redis框架中有注册一个StringRedisTemplate,避免注入冲突
     * @param redisConnectionFactory
     * @return
     */
    @Bean("redisTemplate")
    @Primary
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        
        FastJsonRedisSerializer<Object> fastJsonRedisSerializer = new FastJsonRedisSerializer<>(Object.class);
        // 全局开启AutoType，不建议使用
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);


        // 设置值（value）的序列化采用FastJsonRedisSerializer。
        redisTemplate.setValueSerializer(fastJsonRedisSerializer);
        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);
        // 设置键（key）的序列化采用StringRedisSerializer。
        redisTemplate.setKeySerializer(fastJsonRedisSerializer);
        redisTemplate.setHashKeySerializer(fastJsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    @Lazy
    public RedisCacheManager cacheManager(RedisTemplate redisTemplate) {
        RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate);
        // 开启使用缓存名称最为key前缀
        redisCacheManager.setUsePrefix(true);
        //这里可以设置一个默认的过期时间 单位是秒  默认1天
        redisCacheManager.setDefaultExpiration(86400);

//        // 设置缓存的过期时间
//        Map<String, Long> expires = new HashMap<>();
//        expires.put("people", 1000L);
//        redisCacheManager.setExpires(expires);
        Map<String, Long> expires = new HashMap<>();
        CacheTableService cacheTableService= ApplicationContextUtils.getBean(CacheTableService.class);
        List<CacheTable> cacheTables= cacheTableService.getCacheTableList();
        if(!CollectionUtils.isEmpty(cacheTables)){
            cacheTables.forEach(item->expires.put(item.getName(),item.getCacheTime()));
        }
        if(!CollectionUtils.isEmpty(expires)) {
            redisCacheManager.setExpires(expires);
        }
        return redisCacheManager;
    }


}
