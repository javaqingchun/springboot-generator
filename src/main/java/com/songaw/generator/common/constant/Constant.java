package com.songaw.generator.common.constant;

/**
 * TODO
 *
 * @author songaw
 * @date 2018/7/26 15:07
 */
public class Constant {
    /**
     * 删除标识0：可用
     */
    public static final String CODE_DELETE_FLAG_0 = "0";

    /**
     * 删除标识1：已删
     */
    public static final String CODE_DELETE_FLAG_1 = "1";
    public static final String CODE_CACHE_USERNAME="code_cache_username_";
    public static final String CODE_CACHE_TOKEN_USERNAME="code_cache_token_username_";

    /** 报告类型 */
    public static enum ReportType {
        ALLRP("1"), //1:季度报+半年报+年报
        HANDYEARRP("2"), //2:半年报+年报/半年报/年报
        QUARTERRP("3"), //3:季度报
        NHANDYEARRP("4"); //4:半年报+年报/半年报/年报(无二四季度报)

        private ReportType(String value) {
            this.value = value;
        }

        private final String value;

        public String getValue() {
            return value;
        }

    }

    /** 数据排序规则 */
    public static enum SortRule {
        ASC("asc"), //升序
        DESC("desc"); //降序

        private SortRule(String value) {
            this.value = value;
        }

        private final String value;

        public String getValue() {
            return value;
        }

    }

    //setup_user_id
    public static final Long USER_SYS_CODE=111111111111111111L;
    public static final String USER_SYS_NAME="SYS";

    public static final String ORDER_DESC="DESC";
    public static final String ORDER_ASC="ASC";

    //股票主动型基金银河二级分类代码
    public static final String STOCK_INITIATIVE = "200101,200102,200108,200109,200110,200120";
    //其他型基金银河一级分类代码
    public static final String OTHERS = "2004,2006,2010";
    //债券型代码一级分类代码
    public static final String BOND = "2003";
    //混合型代码一级分类代码
    public static final String MIXED = "2002";
    //股票被动型基金银河二级分类代码
    public static final String STOCK_PASSIVE = "200107";
    //货币型代码一级分类代码
    public static final String CURRENCY = "2005";
}
