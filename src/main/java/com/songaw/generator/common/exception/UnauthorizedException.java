package com.songaw.generator.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * token失效异常
 *
 * @author songaw
 * @date 2018/10/10 15:11
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends BaseException {

    private static final long serialVersionUID = -1490728564509176810L;

    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(String errCode) {
        super(errCode);
    }

    public UnauthorizedException(Throwable throwable) {
        super(throwable);
    }

    public UnauthorizedException(String errCode, Throwable throwable) {
        super(errCode, throwable);

    }
}