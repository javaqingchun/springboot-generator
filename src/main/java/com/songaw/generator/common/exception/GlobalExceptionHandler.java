package com.songaw.generator.common.exception;

import com.songaw.generator.common.pojo.dto.Result;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.ognl.MethodFailedException;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局捕获
 */
@ControllerAdvice
@RestController
public class GlobalExceptionHandler /*extends ResponseEntityExceptionHandler*/  {

    protected final Log logger = LogFactory.getLog(getClass());
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<Result> handleBadRequestException(HttpServletRequest request, HttpServletResponse response, BadRequestException ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseEntity<>(ex.getResult(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public ResponseEntity<Result> handleBusinessException(HttpServletRequest request, HttpServletResponse response, BusinessException ex) {

        Throwable orgEx = ex.getOriginalException();
        if(orgEx!=null ){
            logger.error(orgEx.getMessage(), orgEx);
        }
      //  log.error(ex.getMessage(), ex);
        if(ex.getResult() == null){
            BusinessException ne = new BusinessException("error.500");
            return new ResponseEntity<>(ne.getResult(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            return new ResponseEntity<>(ex.getResult(), HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @ResponseBody
    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<Result> AuthenticationExceptionHandler(UsernameNotFoundException e) {
        String msg="用户找不到!";
      //  logger.info(e.getMessage());
        return new ResponseEntity<Result>(  new Result("error.401",msg), HttpStatus.UNAUTHORIZED);
    }

    @ResponseBody
    @ExceptionHandler(value = UnauthorizedException.class)
    public ResponseEntity<Result> UnauthorizedExceptionHandler(UnauthorizedException e) {
        String msg="凭证失效!";
        //  logger.info(e.getMessage());
        return new ResponseEntity<Result>(  new Result("error.401",msg), HttpStatus.UNAUTHORIZED);
    }
    @ResponseBody
    @ExceptionHandler(value = BadCredentialsException.class)
    public ResponseEntity<Result> AuthenticationExceptionHandler(BadCredentialsException e) {
        String msg="账户或密码错误!";
      //  logger.info(e.getMessage());
        return new ResponseEntity<Result>( new Result("error.401",msg), HttpStatus.UNAUTHORIZED);
    }
    @ResponseBody
    @ExceptionHandler(value = AuthenticationException.class)
    public ResponseEntity<Result> AuthenticationExceptionHandler(AuthenticationException e) {

        return new ResponseEntity<Result>(new Result("error.403"), HttpStatus.FORBIDDEN);
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Result> ExceptionHandler(Exception e) throws Exception {
     e.printStackTrace();
     return new ResponseEntity<Result>( new Result("error.500"), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ResponseBody
    @ExceptionHandler(value = MethodFailedException.class)
    public ResponseEntity<Result> methodFailedException(MethodFailedException e) throws Exception {

        return new ResponseEntity<Result>(  new Result("error.500"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({
            org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException.class,
            HttpRequestMethodNotSupportedException.class,
            HttpMediaTypeNotSupportedException.class,
            HttpMediaTypeNotAcceptableException.class,
            MissingPathVariableException.class,
            MissingServletRequestParameterException.class,
            ServletRequestBindingException.class,
            ConversionNotSupportedException.class,
            TypeMismatchException.class,
            HttpMessageNotReadableException.class,
            HttpMessageNotWritableException.class,
            MethodArgumentNotValidException.class,
            MissingServletRequestPartException.class,
            BindException.class,
            NoHandlerFoundException.class,
            AsyncRequestTimeoutException.class
    })
    public final ResponseEntity<Result> handleException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();

        logger.warn(ex.getMessage());
        if (ex instanceof org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException) {
            HttpStatus status = HttpStatus.NOT_FOUND;

            return new ResponseEntity<Result>(new Result("error.404",ex.getLocalizedMessage()),status);
          //  return handleNoSuchRequestHandlingMethod((org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException) ex, headers, status, request);
        }
        else if (ex instanceof HttpRequestMethodNotSupportedException) {
            HttpStatus status = HttpStatus.METHOD_NOT_ALLOWED;

            return new ResponseEntity<Result>(new Result("error.405",ex.getLocalizedMessage()),status);
         //   return handleHttpRequestMethodNotSupported((HttpRequestMethodNotSupportedException) ex, headers, status, request);
        }
        else if (ex instanceof HttpMediaTypeNotSupportedException) {
            HttpStatus status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
            return new ResponseEntity<Result>(new Result("error.415",ex.getLocalizedMessage()),status);
           // return handleHttpMediaTypeNotSupported((HttpMediaTypeNotSupportedException) ex, headers, status, request);
        }
        else if (ex instanceof HttpMediaTypeNotAcceptableException) {
            HttpStatus status = HttpStatus.NOT_ACCEPTABLE;
            return new ResponseEntity<Result>( new Result("error.406",ex.getLocalizedMessage()),status);
           // return handleHttpMediaTypeNotAcceptable((HttpMediaTypeNotAcceptableException) ex, headers, status, request);
        }
        else if (ex instanceof MissingPathVariableException) {
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<Result>(new Result("error.500",ex.getLocalizedMessage()),status);
           // return handleMissingPathVariable((MissingPathVariableException) ex, headers, status, request);
        }
        else if (ex instanceof MissingServletRequestParameterException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<Result>( new Result("error.400",ex.getLocalizedMessage()),status);
          //  return handleMissingServletRequestParameter((MissingServletRequestParameterException) ex, headers, status, request);
        }
        else if (ex instanceof ServletRequestBindingException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<Result>(new Result("error.400",ex.getLocalizedMessage()),status);
           // return handleServletRequestBindingException((ServletRequestBindingException) ex, headers, status, request);
        }
        else if (ex instanceof ConversionNotSupportedException) {
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<Result>(new Result("error.500",ex.getLocalizedMessage()),status);
          //  return handleConversionNotSupported((ConversionNotSupportedException) ex, headers, status, request);
        }
        else if (ex instanceof TypeMismatchException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<Result>( new Result("error.400",ex.getLocalizedMessage()),status);
           // return handleTypeMismatch((TypeMismatchException) ex, headers, status, request);
        }
        else if (ex instanceof HttpMessageNotReadableException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<Result>( new Result("error.400",ex.getLocalizedMessage()),status);
           // return handleHttpMessageNotReadable((HttpMessageNotReadableException) ex, headers, status, request);
        }
        else if (ex instanceof HttpMessageNotWritableException) {
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<Result>( new Result("error.500",ex.getLocalizedMessage()),status);
            //return handleHttpMessageNotWritable((HttpMessageNotWritableException) ex, headers, status, request);
        }
        else if (ex instanceof MethodArgumentNotValidException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
          //  return new ResponseEntity<Result>( new Result("error.400",ex.getLocalizedMessage()),status);
           return handleMethodArgumentNotValid((MethodArgumentNotValidException) ex, headers, status, request);
        }
        else if (ex instanceof MissingServletRequestPartException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<Result>(  new Result("error.400",ex.getLocalizedMessage()),status);
          // return handleMissingServletRequestPart((MissingServletRequestPartException) ex, headers, status, request);
        }
        else if (ex instanceof BindException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
           // return new ResponseEntity<Result>( new Result("error.400",ex.getLocalizedMessage()),status);
            return handleBindException((BindException) ex, headers, status, request);
        }
        else if (ex instanceof NoHandlerFoundException) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<Result>( new Result("error.404",ex.getLocalizedMessage()),status);
           // return handleNoHandlerFoundException((NoHandlerFoundException) ex, headers, status, request);
        }
        else if (ex instanceof AsyncRequestTimeoutException) {
            HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;
            return new ResponseEntity<Result>( new Result("error.500",ex.getLocalizedMessage()),status);
           /* return handleAsyncRequestTimeoutException(
                    (AsyncRequestTimeoutException) ex, headers, status, request);*/
        }
        else {
            if (logger.isWarnEnabled()) {
                logger.warn("Unknown exception type: " + ex.getClass().getName());
            }
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
           // return new ResponseEntity<Result>( new Result("error.500",ex.getLocalizedMessage()),status);
           return handleExceptionInternal(ex, null, headers, status, request);
        }
    }

    protected ResponseEntity<Result> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        BindingResult result = ex.getBindingResult();
        BadRequestException brex = new BadRequestException(result);

        return handleExceptionInternal(ex, brex.getResult(), headers, status, request);
    }
    protected ResponseEntity<Result> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        BindingResult result = ex.getBindingResult();
        BadRequestException brex = new BadRequestException(result);

        return handleExceptionInternal(ex, brex.getResult(), headers, status, request);
    }
    protected ResponseEntity<Result> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }

        logger.error(ex.getMessage(), ex);
        if(body instanceof Result) {
            Result messages = (Result) body;
            return new ResponseEntity<>(messages, headers, status);
        }
        else {
            return new ResponseEntity<>(new Result("error.500"), headers, status);
        }

    }
}
