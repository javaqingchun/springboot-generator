package com.songaw.generator.common.exception;

import com.songaw.generator.common.pojo.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * base异常
 */
@Slf4j
public abstract class BaseException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -1800374899031616535L;



    //当status为3xx, 4xx, 5xx的时候返回错误消息对象
    private Result result = null;

    private String errorCode = null;


    private Throwable originalException = null;


    /**
     * 构造函数
     */
    public BaseException() {
    }

    /**
     * 构造函数
     *
     * @param errCode
     * @param message
     */
    public BaseException(String errCode, String message) {
        super(message);
        setMessage(errCode, "", message);
    }

    /**
     * 构造函数
     *
     * @param errCode
     */
    public BaseException(String errCode) {
        String message = MessageUtil.getMessage(errCode);
        setMessage(errCode, "", message);
    }

    /**
     * 构造函数
     *
     * @param errCode
     * @param e
     */
    public BaseException(String errCode, Throwable e) {
        this(errCode, MessageUtil.getMessage(errCode));
        originalException = e;
    }

    /**
     * 构造函数
     *
     * @param e
     */
    public BaseException(Throwable e) {
        originalException = e;
       if(e !=null&&e instanceof BaseException){
           BeanUtils.copyProperties(e,this);
           this.result = ((BaseException) e).getResult();
       }else{

           setMessage("error.500", "", e.getMessage());
       }

    }




    public Throwable getOriginalException() {
        return originalException;
    }



    public void setMessage(String messageCode, Object... values) {
        this.errorCode = messageCode;
        for(Object obj : values){
            if(obj instanceof  Throwable){
                this.originalException=(Throwable) obj;
                break;
            }
        }
        Result result = getResult();
        result.setCode(messageCode);
        result.setMessage( MessageUtil.getMessage(messageCode, values));
    }


    /*public void setMessage(String messageCode, String message) {
        Result result = getResult();
        result.code(messageCode);
        result.setMessage(message);
        this.errorCode = messageCode;
    }*/

    public Result getResult() {
        if (result == null)
            result = new Result();
        return result;

    }




    /**
     * 获取消息字符串
     *
     * @return 消息字符串
     * @see Throwable#getMessage()
     */
    public String getMessage() {

        if (result != null) {
           return  result.getMessage();
        }
       return "";
    }

    /**
     * 异常实例转String，返回消息字符串
     *
     * @return 消息字符串
     * @see Object#toString()
     */
    public String toString() {
        return getMessage();
    }


    public static String get(Throwable t) {
        //
        if (t == null) {
            return "NULL";
        }
        // Create
        StringBuffer b = new StringBuffer();
        //
        b.append(t.getMessage());
        b.append("\n");
        //
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        //
        t.printStackTrace(ps);
        //
        b.append(baos.toString());
        //
        return b.toString();
    }

    public String getErrorCode() {
        return errorCode;
    }


    public void setOriginalException(Throwable originalException) {
        this.originalException = originalException;
    }

}
