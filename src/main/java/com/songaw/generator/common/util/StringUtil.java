package com.songaw.generator.common.util;

/**
 * 字符串操作工具类
 * @author wh
 * @create 2018-09-11 13:36
 */
public class StringUtil
{

    /**
     * 字符串为空则返回0
     * @param str
     * @return
     */
    public static String Nvl(String str){
        return str == null ? "0" : str;
    }

}
