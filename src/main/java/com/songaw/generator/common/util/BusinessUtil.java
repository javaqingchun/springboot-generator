package com.songaw.generator.common.util;

import com.songaw.generator.common.constant.Constant;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by admin on 2018/9/27.
 */
public final class BusinessUtil {
    /**
     * 求分位数
     * @param bd
     * @param list
     * @param orderby default asc
     * @return
     */
    public static BigDecimal getFractile(BigDecimal bd, List<BigDecimal> list, String orderby ){

        String oby = Constant.ORDER_ASC;
        if( !StringUtils.isEmpty(orderby) ){
            oby = orderby;
        }

        if( bd == null )return BigDecimal.ZERO;
        int smallCount = 0;
        for (BigDecimal bigDecimal : list) {
            if( Constant.ORDER_ASC.equalsIgnoreCase(oby) ){
                if( bigDecimal.compareTo(bd) < 0 ){
                    smallCount ++;
                }else {
                    break;
                }
            }else {
                if( bigDecimal.compareTo(bd) > 0 ){
                    smallCount ++;
                }else {
                    break;
                }
            }
        }

        return new BigDecimal((float)smallCount/(list.size()-1) ).setScale(5, BigDecimal.ROUND_HALF_UP);
    }
}
