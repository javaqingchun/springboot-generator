package com.songaw.generator.common.util;

import com.songaw.generator.modules.auths.pojo.dto.AuthUserDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 用戶操作
 *
 * @author songaw
 * @date 2018/9/6 9:13
 */
public class UserUtil {
    public static Long getUserId(){
        AuthUserDto authUserDto =getUser();
        if(authUserDto!=null){
            return authUserDto.getId();
        }
        return null;
    }
    public static AuthUserDto getUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            AuthUserDto userDetails = (AuthUserDto) auth.getPrincipal();
            return userDetails;
        }
        return null;
    }
}
