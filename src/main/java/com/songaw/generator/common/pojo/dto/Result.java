package com.songaw.generator.common.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.songaw.generator.common.exception.MessageUtil;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Result<T> implements Serializable {

	private static final long serialVersionUID = -1872625368730485194L;
	public static final String ERR_500 = "error.500"; //system error
	public static final String ERR_400 = "error.400"; //参数错误
	public static final String ERR_401 = "error.401"; //登陆失败
	public static final String ERR_403 = "error.403"; //权限错误
	public static final String ERR_404 = "error.404"; //no found
	public static final String ERR_405 = "error.405"; //no found
	public static final String SUCCESS_200 = "200"; //成功
    
    public Result<T> code(String code) {
        this.code = code;
		return this;
	}

	public Result<T> message(String message) {
		this.message = message;
		return this;
	}
	public Result<T> data(T result) {
		this.data = result;
		return this;
	}
    
    public Result(){}

	public Result(String code,String message){
		this.code=code;
		this.message=MessageUtil.getMessage(code);
		if(this.message!=null&&this.message.length()>0){
			this.message=message;
		}
	}
	public Result(String code,String defauleMessage,Object... params){
		this.code=code;
		this.message=MessageUtil.getMessage(code, params);
	}
	public Result(String code){
		this.code=code;
		this.message=MessageUtil.getMessage(code);
	}

    public static<T> Result<T> getSuccessResult(T value) {
		Result<T> rslt = new Result<T>();
		rslt.setCode(SUCCESS_200);
		rslt.setData(value);
		rslt.setMessage(MessageUtil.getMessage(SUCCESS_200));
		return rslt;
    }

	public static<T> Result<T> getSystemErrorResult(String message){
		Result<T> rslt = new Result<T>();
		rslt.setCode(ERR_500);
		rslt.setMessage(message);
		return rslt;
	}

    /** 错误码 **/
	public String code="success.200";
	
	/** 返回信息 **/
    public String message;
    
    /** 返回对象 存放具体的json数据 **/
    public T data;
    
    @JsonProperty("success")
	public boolean getSuccess() {
		return this.code == SUCCESS_200;
	}

}