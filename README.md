spring boot 快速开发脚手架 集成redis tk.mybatis 代码生成工具,jwt_token,Sspring Security 帮助快速开发

我们公司项目前端使用的是react 前后端分离  

  我自己用jquery写了一遍后台的简单刷新机制  
这个是个demo环境  主要写了一些token刷新的思路  定时缓存的思路  生成代码的一些方法 
异常捕捉的规范 
2018-10-11 新增缓存管理,维护token刷新机制

使用方法  
http://localhost:8080/api  里面存的是所有的接口

到权限管理接口里面注册个用户  并配置个角色

扩展的tk.mybatis.mapper   可以生成 可以生成 entity mapper  service  controller 遵循restful 


mvn mybatis-generator:generate  生成代码
![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/171648_71441151_504009.png "屏幕截图.png")

结合 mybatis3plugins项目使用 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/171347_05bad484_504009.png "屏幕截图.png")

spring security
![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/171448_1b9009c7_504009.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0806/171512_c1ea8e2c_504009.png "屏幕截图.png")
![缓存管理](https://images.gitee.com/uploads/images/2018/1011/155538_caaaa273_504009.png "屏幕截图.png")